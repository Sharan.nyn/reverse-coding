/*
 * Magic square for odd and even degree of natural numbers
 */

#include <iostream>
#include <cstring>
#include <cstdio>

using namespace std;

void oddSquare(int n)
{
	int magicSquare[n][n];
	memset(magicSquare, 0, sizeof(magicSquare));
	int i(n/2), j(n-1);
	for (int num=1; num <= n*n; ) {
		if (i==-1 && j==n) {
			j = n-2;
			i = 0;
		} else {
			if (j == n) j = 0;
			if (i < 0)	i=n-1;
		}
		if (magicSquare[i][j]) {
			j -= 2;
			i++;
			continue;
		}
		else
			magicSquare[i][j] = num++;
		j++; i--;
	}

	for (i=0; i<n; i++) {
		for (j=0; j<n; j++)
			printf("%3d ", magicSquare[i][j]);
		cout << '\n';
	}
}

void evenSquare(int n) {
	int arr[n][n], i, j;

	for ( i = 0; i < n; i++)
		for ( j = 0; j < n; j++)
			arr[i][j] = (n*i) + j + 1;

	for ( i = 0; i < n/4; i++)
		for ( j = 0; j < n/4; j++)
			arr[i][j] = (n*n + 1) - arr[i][j];

	for ( i = 0; i < n/4; i++)
		for ( j = 3 * (n/4); j < n; j++)
			arr[i][j] = (n*n + 1) - arr[i][j];

	for ( i = 3 * n/4; i < n; i++)
		for ( j = 0; j < n/4; j++)
			arr[i][j] = (n*n+1) - arr[i][j];

	for ( i = 3 * n/4; i < n; i++)
		for ( j = 3 * n/4; j < n; j++)
			arr[i][j] = (n*n + 1) - arr[i][j];

	for ( i = n/4; i < 3 * n/4; i++)
		for ( j = n/4; j < 3 * n/4; j++)
			arr[i][j] = (n*n + 1) - arr[i][j];

	for (i = 0; i < n; i++) {
		for ( j = 0; j < n; j++)
			printf("%3d ", arr[i][j]);
		cout << "\n";
	}
}

void square(int num) {
	if(num == 2){
		cout<<"ERROR!\n\n";
		return;
	}
	if (num & 1) oddSquare (num);
	else evenSquare(num) ;
}

void CheckNum() {
	string input;
	long num(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += int(s-'0');
		}
	}
	square(num);
	cout << "\n\n";
}

int main() {
    cout << "Clue: \"We think magic is very much related to happiness. Narayana Pandit is also agreed on this when he states - The purpose of studying \"this\" magic is to destroy the ego of bad mathematicians, and for the pleasure of good mathematicians.\"\n";
	while(1)
		CheckNum();
	return 0;
}