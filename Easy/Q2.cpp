/*
 * Print yes/no for palindrome strings/integers.
 */

#include <iostream>
#include <algorithm>

using namespace std;

int main() {
	string input;
	cout << "Clue: \"Murder For A Jar Of Red Rum\"\n";
	while(1){
		getline(cin, input);
		string reversed(input);
		reverse(reversed.begin(), reversed.end());
		if(input.compare(reversed) == 0) cout << "Yes\n\n";
		else cout << "No\n\n";
	}
	return 0;
}