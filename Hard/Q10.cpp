/*
 * Finding median of running stream of data.
 */

#include <queue> 
#include <iostream> 
#include <limits>

using namespace std; 
class MedianFinder {
	priority_queue<long> small, large;
public:
	void addNum(long num) {
		small.push(num);
		large.push(-small.top());
		small.pop();
		if (small.size() < large.size()) {
			small.push(-large.top());
			large.pop();
		}
	}

	double findMedian() {
		return small.size() > large.size()
			   ? small.top()
			   : (small.top() - large.top()) / 2.0;
	}
};
int main() 
{
	long element;
	cout << "Clue: \"The rains aren’t ceasing and a deluge is imminent! The clock is ticking while you scamper to find your ground amidst this stream!\" \n";
	MedianFinder mf;
	while(true){
		if (cin >> element) {
			mf.addNum(element);
			cout << mf.findMedian() << "\n\n";
		} else {
			cout<<"ERROR!\n\n";
			cin.clear();
			cin.ignore(numeric_limits<streamsize>::max(), '\n');
		}
	}
	return 0; 
} 
