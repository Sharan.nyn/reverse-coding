/*
 * Sum of a number and square of that number.
 */

#include <iostream>

using namespace std;
void sumNumberSquare(){
	string input;
	unsigned long num(0), square(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += (unsigned long)(s-'0');
		}
	}
	square = num * num;
	cout << square + num << "\n\n";
}
int main() {
	cout << "Clue: \"Be fruitful and multiply\"\n";
	while(1)
		sumNumberSquare();
	return 0;
}