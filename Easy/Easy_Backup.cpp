/*
 * Double Factorial.
 */

#include <iostream>
#include <limits>
using namespace std;
// C++ program to compute factorial of big numbers 
#include<iostream> 
using namespace std; 

// Maximum number of digits in output 
#define MAX 500 

int multiply(int x, int res[], int res_size); 

// This function finds factorial of large numbers 
// and prints them 
void factorial(int n, bool neg) 
{ 
	int res[MAX]; 

	// Initialize result 
	res[0] = 1; 
	int res_size = 1; 

	// Apply simple factorial formula n! = 1 * 2 * 3 * 4...*n 
	for (int x=n; x>=1; x-=2) 
		res_size = multiply(x, res, res_size); 
	if(neg) cout << '-';
	for (int i=res_size-1; i>=0; i--) 
		cout << res[i]; 
	cout << "\n\n";
} 

// This function multiplies x with the number 
// represented by res[]. 
// res_size is size of res[] or number of digits in the 
// number represented by res[]. This function uses simple 
// school mathematics for multiplication. 
// This function may value of res_size and returns the 
// new value of res_size 
int multiply(int x, int res[], int res_size) 
{ 
	int carry = 0; // Initialize carry 

	// One by one multiply n with individual digits of res[] 
	for (int i=0; i<res_size; i++) 
	{ 
		int prod = res[i] * x + carry; 

		// Store last digit of 'prod' in res[] 
		res[i] = prod % 10; 

		// Put rest in carry 
		carry = prod/10;	 
	} 

	// Put carry in res and increase result size 
	while (carry) 
	{ 
		res[res_size] = carry%10; 
		carry = carry/10; 
		res_size++; 
	} 
	return res_size; 
} 

void checkNum(){
	int num(0);
	bool negative_flag=false;
	if (cin >> num) {
		if(num < 0){
			negative_flag = true;
			num *= -1;
		}
		if(num > 450){
			cout << "Number too big!\n\n"; return;
		}
		factorial(num, negative_flag);
	} else {
		cout<<"ERROR!\n\n";
		cin.clear();
		cin.ignore(numeric_limits<streamsize>::max(), '\n');
	}
}
int main() {
	cout << "Clue : \"Double Trouble!!\"\n\n";
	while(1)
		checkNum();
	return 0;
}
