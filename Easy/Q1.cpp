/*
 * Sum of digits/letters in ASCII and printing it in base5.
 */

#include <iostream>
#include <algorithm>

using namespace std;

void baseConversion(unsigned long num, unsigned long base){
	string stringResult = "";
	while(num){
		stringResult += ' ';
		stringResult += (char)((num % base) + '0');
		num /= base;
	}
	reverse(stringResult.begin(), stringResult.end());
	cout<< stringResult << "\n\n";
}

int main() {
	string input;
	cout << "Clue: \"All your base are belong to us!\"\n";
	while(1){
		cin>>input;
		unsigned long asciisum = 0;
		for(auto c:input){
			asciisum += (unsigned long) c;
		}
		baseConversion(asciisum, 5);
	}
	return 0;
}
