/*
 * Zeckendorf’s Theorem (Non-Neighbouring Fibonacci Representation)
 */

#include <iostream>

using namespace std;
unsigned long getClosestFibonacci(unsigned long num){
	unsigned long a(0), b(1), res(0);
	while(1){
		if( a <= num - b){
			res = a+b;
			a = b;
			b = res;
		} else {
			cout << res << ' ';
			return res;
		}
	}
}
void Zeckendorf(){
	string input;
	unsigned long num(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += int(s-'0');
		}
	}
	if(num <= 1) {
		cout << num << "\n\n";
		return;
	}
	while(num)
		num -= getClosestFibonacci(num);
	cout << "\n\n";
}
int main() {
	cout << "Clue: \"The essence of mathematics knows no boundaries. And this Belgian doctor, army officer and mathematician is a personification of that statement.\"\n";
	while(1)
		Zeckendorf();
	return 0;
}