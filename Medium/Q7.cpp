/*
 * Sum of unique prime factors of the given number.
 */

#include <iostream>

using namespace std;
int Sum(int N) {
	int sumOfPrimeDivisors[N+1]={0};
	for (int i = 2; i <= N; ++i) {
		// if the number is prime
		if (!sumOfPrimeDivisors[i]) {
			// add this prime to all it's multiples
			for (int j = i; j <= N; j += i)
				sumOfPrimeDivisors[j] += i;
		}
	}
	return sumOfPrimeDivisors[N];
}

void CheckNum() {
	string input;
	int num(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += (int)(s-'0');
		}
	}
	cout<<Sum(num)<<"\n\n";
}

int main() {
	cout << "Clue: \"Time for some elementary school mathematics! The need of the hour is to tally the loners in the fundamental theorem.\"\n";
	while(1)
		CheckNum();
	return 0;
}
