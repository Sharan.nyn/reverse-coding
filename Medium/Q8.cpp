/*
 * Reverse the binary bits of a number
 */

#include <iostream>

using namespace std;

unsigned int reverseBits(unsigned int num)
{
	unsigned int count = sizeof(num) * 8 - 1;
	unsigned int reverse_num = num;

	num >>= 1;
	while(num)
	{
		reverse_num <<= 1;
		reverse_num |= num & 1;
		num >>= 1;
		count--;
	}
	reverse_num <<= count;
	return reverse_num;
}
void checkNum(){
	string input;
	unsigned int num(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += (unsigned int)(s-'0');
		}
	}
	cout << reverseBits(num) << "\n\n";
}
int main() {
	string input;
	cout << "Clue: \"There are 32 units in a computer who are bored. To brighten things around, they decide to manipulate themselves by following the event theme. \"\n";
	while(1)
		checkNum();
	return 0;
}
