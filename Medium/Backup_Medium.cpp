/*
 * Print all possible interpretations of an array of digits
 */

#include <iostream>
#include <vector>
using namespace std;

const char alphabet[27] = {' ', 'a', 'b', 'c', 'd', 'e',
		'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
		's', 't', 'u', 'v', 'w', 'x', 'v', 'z'};

class Node {
public:
	string dataString;
	Node* left;
	Node* right;
	Node(string dataString) {
		this->dataString = dataString;
		left = NULL;
		right = NULL;
	}
};

Node* createTree(int data, string pString, vector<int> arr) {
	if (data > 26)
		return NULL;
	string dataToStr = pString + alphabet[data];
	Node *root = new Node(dataToStr);
	if (arr.size() != 0) {
		data = arr[0];
		vector<int> newArr;
		for(int i = 1; i < arr.size(); i++) newArr.push_back(arr[i]);
		root->left = createTree(data, dataToStr, newArr);
		if (arr.size() > 1) {
			data = arr[0] * 10 + arr[1];
			vector<int> newArr;
			for(int i = 2; i < arr.size(); i++) newArr.push_back(arr[i]);
			root->right = createTree(data, dataToStr, newArr);
		}
	}
	return root;
}

void printleaf(Node *root) {
	if(root == NULL) return;
	if (root->left == NULL && root->right == NULL)
		cout << root->dataString << ' ';

	printleaf(root->left);
	printleaf(root->right);
}

void printAllInterpretations(vector<int> arr) {

	// Step 1: Create Tree
	Node *root = createTree(0, "", arr);

	// Step 2: Print Leaf nodes
	printleaf(root);

	cout << "\n\n";
}

void checkNum(){
	string input;
	vector <int> test;
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else test.push_back(s-'0');
	}
	printAllInterpretations(test);
}

int main(){
	// cout << "Clue: \"What is 987? It's nine hundred eighty-seven\"\n\n";
	while(1) checkNum();
	return 0;
}