/*
 * Look-say sequence.
 */

#include <iostream>
using namespace std;

string countAndSay(unsigned long n) {
	if(n == 1) return "1";
	string result = "";
	string input = "1";
	for(unsigned long j=0; j<n-1; j++){
		result = "";
		char num = input[0];
		unsigned long nCount=1;
		for(unsigned long i=1; i<input.size(); i++){
			if(input[i] == num)
				nCount++;
			else {
				result += to_string(nCount) + num;
				nCount = 1;
				num = input[i];
			}
		}
		result += to_string(nCount) + num;
		input = result;
	}
	return result;
}

void lookAndSay() {
	string input;
	unsigned long num(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += (unsigned long)(s-'0');
		}
	}
	cout<<countAndSay(num)<<"\n\n";
}

int main() {
	cout << "Clue: \"Just smile and wave, boys. Smile and wave.\", said Skipper the Penguin. I replied, \"But Skipper, all they've got to do is to just look and say.\" Say what? That's for you to figure out!\"\n";
	while(1)
		lookAndSay();
	return 0;
}