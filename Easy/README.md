# Reverse Coding - Easy

Easy questions decided so far:
1. Sum of digits/letters in ASCII and printing it in base5.
2. Print yes/no for palindrome strings/integers.
3. Difference of 1s and 0s in binary form for numbers.
4. Sum of a number and square of that number.

5. Backup - Difference of the factorial and the number itself.