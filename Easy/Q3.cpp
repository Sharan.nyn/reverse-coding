/*
 * Difference of 1s and 0s in binary form for numbers.
 */

#include <iostream>

using namespace std;
void diff1s0s(){
	string input;
	unsigned long num(0), ones(0), zeros(0);
	cin >> input;
	for(auto s:input){
		if(isdigit(s)!=1){
			cout<<"ERROR!\n\n";
			return;
		}
		else{
			num *= 10;
			num += (unsigned long)(s-'0');
		}
	}
	while(num){
		if(num & 1) ones++;
		else zeros++;
		num = num >> 1;
	}
	cout << (int)(ones - zeros) << "\n\n";
}
int main() {
	cout << "Clue: \"Beep Beep Boop Boop ?\"\n";
	while(1)
		diff1s0s();
	return 0;
}