# Reverse Coding - Medium

Medium questions decided so far:
1. Look-say sequence.
2. Zeckendorf’s Theorem (Non-Neighbouring Fibonacci Representation).
3. Sum of unique prime factors of the given number.
4. Reverse the binary bits of a 32 bit number.

5. Backup - Print all possible interpretations of an array of digits.